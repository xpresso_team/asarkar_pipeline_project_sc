#! /bin/bash
## This script is used to build the docker image for the project
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user

MODE=${1}
current_folder=${ROOT_FOLDER}/xprbuild/docker

cmd="docker build -t ${PROJECT_NAME}:${PROJECT_VERSION} -f $current_folder/Dockerfile ${ROOT_FOLDER}"
echo "Building the docker image"
echo "Docker build command"
exec $cmd

